const val kotlinVersion = "1.2.51"
const val androidPluginVersion = "3.2.0"
const val dexCountVersion = "0.8.2"
const val googlePlayVersion = "4.0.1"

// Compile dependencies
const val appCompatVersion = "28.0.0"
const val constraintLayoutVersion = "1.1.2"
const val archExtensionVersion = "1.1.1"
const val archCompVersion = "1.1.1"
const val timberVersion = "4.7.1"
const val butterknifeVersion = "9.0.0-rc1"
const val leakCanaryVersion = "1.5.4"
const val retrofitVersion = "2.3.0"
const val okhttpVersion = "3.9.0"
const val rxJava2Version = "2.1.6"
const val rxKotlinVersion = "2.1.0"
const val rxAndroidVersion = "2.0.1"
const val rxBindingVersion = "2.0.0"
const val rxMathVersion = "0.20.0"
const val daggerVersion = "2.18"
const val multiDexVersion = "1.0.1"
const val glideVersion = "4.7.1"
const val glassFishVersion = "10.0-b28"
const val designVersion = appCompatVersion
const val epoxyVersion = "2.19.0"
const val circleImageViewVersion = "2.2.0"
const val snapHelperVersion = "1.5"
const val runtimePermissionVersion = "0.10.2"
const val geoFireVersion = "2.3.1"
const val readMoreTextViewVersion = "2.1.0"
const val threeTenABPVersion = "1.1.1"

// Test dependency versions
const val junitVersion = "4.12"
const val testRunnerVersion = "1.0.1"
const val espressoVersion = "3.0.1"

// https://proandroiddev.com/gradle-dependency-management-with-kotlin-94eed4df9a28

object BuildPlugins {
    val androidPlugin = "com.android.tools.build:gradle:$androidPluginVersion"
    val kotlinPlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion"
    val dexCountPlugin = "com.getkeepsafe.dexcount:dexcount-gradle-plugin:$dexCountVersion"
    val googlePlayPlugin = "com.google.gms:google-services:$googlePlayVersion"
}

object Android {
    // Manifest version information!
    private const val versionMajor = 0
    private const val versionMinor = 0
    private const val versionPatch = 0
    private const val versionBuild = 1 // bump for dogfood builds, public betas, etc.

    const val versionCode =
        versionMajor * 10000 + versionMinor * 1000 + versionPatch * 100 + versionBuild
    const val versionName = "$versionMajor.$versionMinor.$versionPatch"

    const val compileSdkVersion = 28
    const val targetSdkVersion = 28
    const val minSdkVersion = 21
}

object Libs {

    val kotlinStdlb = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlinVersion"
    val appCompat = "com.android.support:appcompat-v7:$appCompatVersion"
    val cardView = "com.android.support:cardview-v7:$appCompatVersion"
    val constraintLayout =
        "com.android.support.constraint:constraint-layout:$constraintLayoutVersion"
    val archExtensions = "android.arch.lifecycle:extensions:$archExtensionVersion"
    val archExtensionsCompiler = "android.arch.lifecycle:compiler:$archExtensionVersion"
    val room = "android.arch.persistence.room:runtime:$archCompVersion"
    val roomRx = "android.arch.persistence.room:rxjava2:$archCompVersion"
    val roomCompiler = "android.arch.persistence.room:compiler:$archCompVersion"
    val archCoreTesting = "android.arch.core:core-testing:$archCompVersion"
    val roomTesting = "android.arch.persistence.room:testing:$archCompVersion"
    val timber = "com.jakewharton.timber:timber:$timberVersion"
    val butterknife = "com.jakewharton:butterknife:$butterknifeVersion"
    val butterknifeCompiler = "com.jakewharton:butterknife-compiler:$butterknifeVersion"
    val retrofit = "com.squareup.retrofit2:retrofit:$retrofitVersion"
    val retrofitRxJava2 = "com.squareup.retrofit2:adapter-rxjava2:$retrofitVersion"
    val retrofitGson = "com.squareup.retrofit2:converter-gson:$retrofitVersion"
    val okhttp = "com.squareup.okhttp3:okhttp:$okhttpVersion"
    val okhttpLogging = "com.squareup.okhttp3:logging-interceptor:$okhttpVersion"
    val rxjava2 = "io.reactivex.rxjava2:rxjava:$rxJava2Version"
    val rxkotlin = "io.reactivex.rxjava2:rxkotlin:$rxKotlinVersion"
    val rxandroid = "io.reactivex.rxjava2:rxandroid:$rxAndroidVersion"
    val rxmath = "com.github.akarnokd:rxjava2-extensions:$rxMathVersion"
    val rxbindings = "com.jakewharton.rxbinding2:rxbinding-kotlin:$rxBindingVersion"
    val dagger = "com.google.dagger:dagger:$daggerVersion"
    val daggerCompiler = "com.google.dagger:dagger-compiler:$daggerVersion"
    val daggerAndroid = "com.google.dagger:dagger-android:$daggerVersion"
    val daggerSupport = "com.google.dagger:dagger-android-support:$daggerVersion"
    val daggerProcessor = "com.google.dagger:dagger-android-processor:$daggerVersion"
    val multiDex = "com.android.support:multidex:$multiDexVersion"
    val glide = "com.github.bumptech.glide:glide:$glideVersion"
    val glideCompiler = "com.github.bumptech.glide:compiler:$glideVersion"
    val glassFishAnnotation = "org.glassfish:javax.annotation:$glassFishVersion"
    val supportDesign = "com.android.support:design:$designVersion"
    val epoxy = "com.airbnb.android:epoxy:$epoxyVersion"
    val epoxyProcessor = "com.airbnb.android:epoxy-processor:$epoxyVersion"
    val circleImageView = "de.hdodenhof:circleimageview:$circleImageViewVersion"
    val snapHelperPlugin = "com.github.rubensousa:gravitysnaphelper:$snapHelperVersion"
    val runtimePermission = "com.github.tbruyelle:rxpermissions:$runtimePermissionVersion"
    val geoFire = "com.firebase:geofire-android:$geoFireVersion"
    val readMoreTextView = "com.borjabravo:readmoretextview:$readMoreTextViewVersion"
    val threeTenABP = "com.jakewharton.threetenabp:threetenabp:$threeTenABPVersion"
}

object TestLibs {
    val junit = "junit:junit:$junitVersion"
    val testRunner = "com.android.support.test:runner:$testRunnerVersion"
    val espresso = "com.android.support.test.espresso:espresso-core:$espressoVersion"
}

