package com.appetiser.baseplateproject.features

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.appetiser.baseplateproject.R
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasActivityInjector {

    override fun activityInjector(): AndroidInjector<Activity> = dispatchingAndroidInjector

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: ScoreViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViewModelandLiveData()
    }

    private fun setupViewModelandLiveData() {
        viewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(ScoreViewModel::class.java)

        viewModel.blueTeamPoints.observe(this, Observer {
            it?.let {
                blueTeam.text = it.score.toString()
            }

        })

        viewModel.redTeamPoints.observe(this, Observer {
            it?.let {
                redTeam.text = it.score.toString()
            }

        })

        btnAddBlue.setOnClickListener {
            viewModel.addPointsToBlueTeam(1)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
        }

        btnAddRed.setOnClickListener {
            viewModel.addPointsToRedTeam(1)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
        }

    }

}
