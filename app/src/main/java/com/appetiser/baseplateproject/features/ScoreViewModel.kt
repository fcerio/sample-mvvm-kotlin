package com.appetiser.baseplateproject.features

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.content.SharedPreferences
import com.appetiser.baseplateproject.data.poko.Score
import com.appetiser.baseplateproject.data.source.repository.ScoreRepository
import io.reactivex.Observable
import javax.inject.Inject

class ScoreViewModel @Inject constructor(private val sharePref: SharedPreferences, private val repository: ScoreRepository) :
    ViewModel() {

    private val _redTeamPoints: MutableLiveData<Score> by lazy {
        MutableLiveData<Score>()
    }

    private val _blueTeamPoints: MutableLiveData<Score> by lazy {
        MutableLiveData<Score>()
    }

    val redTeamPoints: LiveData<Score>
        get() {
            return _redTeamPoints
        }

    val blueTeamPoints: LiveData<Score>
        get() {
            return _blueTeamPoints
        }


    fun addPointsToRedTeam(points: Int): Observable<Score> {
        return repository.addPointsToRedTeam(points)
            .doOnNext {
                var score = _redTeamPoints.value

                if (score != null) {
                    score.score += points
                } else {
                    score = Score(points)
                }

                _redTeamPoints.value = score
            }
    }

    fun addPointsToBlueTeam(points: Int): Observable<Score> {
        return repository.addPointsToRedTeam(points)
            .doOnNext {
                var score = _blueTeamPoints.value

                if (score != null) {
                    score.score += points
                } else {
                    score = Score(points)
                }

                _blueTeamPoints.value = score
            }
    }

}