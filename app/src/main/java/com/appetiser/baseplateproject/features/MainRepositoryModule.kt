package com.appetiser.baseplateproject.features

import com.appetiser.baseplateproject.data.source.remote.ScoreRemoteSource
import com.appetiser.baseplateproject.data.source.repository.ScoreRepository
import com.appetiser.baseplateproject.di.scopes.ActivityScope
import dagger.Module
import dagger.Provides

@Module
class MainRepositoryModule {

    @ActivityScope
    @Provides
    fun providesScoreRemoteSource(): ScoreRemoteSource = ScoreRemoteSource()


    @ActivityScope
    @Provides
    fun providesScoreRepository(remote: ScoreRemoteSource): ScoreRepository =
        ScoreRepository(remote)

}