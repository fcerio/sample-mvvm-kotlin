package com.appetiser.baseplateproject.di

import android.app.Application
import com.appetiser.baseplateproject.App
import com.appetiser.baseplateproject.di.builders.ActivityBuilder
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        (AndroidSupportInjectionModule::class),
        (StorageModule::class),
        (NetworkModule::class),
        (ActivityBuilder::class),
        (ViewModelModule::class)
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: App)
}