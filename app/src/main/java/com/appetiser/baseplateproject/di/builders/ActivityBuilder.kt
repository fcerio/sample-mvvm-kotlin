package com.appetiser.baseplateproject.di.builders

import com.appetiser.baseplateproject.features.MainActivity
import com.appetiser.baseplateproject.di.scopes.ActivityScope
import com.appetiser.baseplateproject.features.MainRepositoryModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector(modules = [(MainRepositoryModule::class)])
    abstract fun contributeMainActivity(): MainActivity

}