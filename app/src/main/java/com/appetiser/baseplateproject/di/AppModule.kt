package com.appetiser.baseplateproject.di

import android.content.Context
import com.appetiser.baseplateproject.App
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class AppModule {

    @Singleton
    @Binds
    abstract fun providesApplicationContext(app: App) : Context
}