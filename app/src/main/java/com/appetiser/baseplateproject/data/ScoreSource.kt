package com.appetiser.baseplateproject.data

import com.appetiser.baseplateproject.data.poko.Score
import io.reactivex.Observable

interface ScoreSource {

    fun addPointsToRedTeam(points: Int): Observable<Score>

    fun addPointsToBlueTeam(points: Int): Observable<Score>

//    fun getRedTeamScore(points: Int): Observable<Score>
//
//    fun getBlueTeamScore(points: Int): Observable<Score>

}