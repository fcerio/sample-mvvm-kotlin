package com.appetiser.baseplateproject.data.source.remote

import com.appetiser.baseplateproject.data.ScoreSource
import com.appetiser.baseplateproject.data.poko.Score
import io.reactivex.Observable
import javax.inject.Inject

class ScoreRemoteSource @Inject constructor() : ScoreSource {


    override fun addPointsToRedTeam(points: Int): Observable<Score> = Observable.just(points).map {
        Score(it + 1)
    }

    override fun addPointsToBlueTeam(points: Int): Observable<Score> = Observable.just(points).map {
        Score(it + 1)
    }

//    override fun getRedTeamScore(points: Int): Observable<Score> = Observable.just(points).map {
//        Score(it)
//    }
//
//    override fun getBlueTeamScore(points: Int): Observable<Score> =
//        Observable.just(points).map { Score(it) }

}