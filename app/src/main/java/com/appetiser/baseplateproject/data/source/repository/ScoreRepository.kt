package com.appetiser.baseplateproject.data.source.repository

import com.appetiser.baseplateproject.data.ScoreSource
import com.appetiser.baseplateproject.data.poko.Score
import com.appetiser.baseplateproject.data.source.remote.ScoreRemoteSource
import io.reactivex.Observable
import javax.inject.Inject

class ScoreRepository @Inject constructor(private val remote: ScoreRemoteSource) : ScoreSource {

//    override fun getRedTeamScore(points: Int): Observable<Score> = remote.getRedTeamScore(points)
//
//    override fun getBlueTeamScore(points: Int): Observable<Score> = remote.getBlueTeamScore(points)

    override fun addPointsToRedTeam(points: Int): Observable<Score> =
        remote.addPointsToRedTeam(points)

    override fun addPointsToBlueTeam(points: Int): Observable<Score> =
        remote.addPointsToBlueTeam(points)

}